# README #

Ordering System Using Augmented Reality

### What is this repository for? ###

* It is Unity3D project made for Human Computer Interaction Class Spring 2016 Taught by Dr. Shaundra Daily.
* It is a Unity project deployable as an Android application. Users can use the application to check out the menu and see the menu item on there table using Augmented Reality.


### How do I get set up? ###

* Make sure you have Unity3D 5.* 32bit. It uses Vuforia SDK to implement AUgmented Reality which only supports 32bit architecture on PCs(may work with Unity64bit with mac) 

contact for details:

Naman Rajpal
namanrajpal16@ufl.edu
+13523262948
Student Developer
University of Florida 2016