﻿using UnityEngine;
using System.Collections;

public class PanelListController : MonoBehaviour {

	public GameObject content;
	public GameObject listitem;
	public GameObject UImanager;


	void Start () {

		foreach (item i in appdata.allitems) {

			if (i.category == appdata.currenttype) {
				GameObject litem = Instantiate (listitem) as GameObject;
				PanelListitemController controller = litem.GetComponent<PanelListitemController> ();
				controller.icon.sprite = i.image;
				controller.title.text = i.name;
				controller.add.gameObject.name = i.name;
				controller.details.gameObject.name = i.name;
				controller.add.onClick.AddListener (() => UImanager.GetComponent<buttoncontrol> ().add ());
				controller.details.onClick.AddListener (() => UImanager.GetComponent<buttoncontrol> ().fetchdetails ());
				litem.transform.parent = content.transform;
				litem.transform.localScale = Vector3.one;
			}

		}




	}
	// Update is called once per frame
	void Update () {
	
	}
}
