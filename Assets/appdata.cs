﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class appdata : MonoBehaviour {

	public static string type; // represents the type of order here
	public static List<string> items;
	public static string currentitem; //item selected for detail page
	public static string currenttype; 
	public static int lastscene;

	public static orderdetails order;

	public static List<item> allitems;
	public static List<item> cart;
	public Sprite[] images;

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
	}
		
	// Use this for initialization
	void Start () {
		type = null;
		items = new List<string> ();
		allitems = new List<item> ();
		cart = new List<item> ();
		order = new orderdetails ();



		allitems.Add(new item(images[0],"Black Forest Cake 1.5lbs","made with Choclate,butter and honey",18.0f,"desserts"));
		allitems.Add(new item(images[1],"Donuts 6 pieces","Choclate or Vaniila Donuts",12.0f,"desserts"));
		allitems.Add(new item(images[2],"Kebabs","Grilled and topped with avocado, roasted gren chillies, pepper jack cheese, mayonnaise and ranch dresing on grilled sourdough bread.\n\nServed with yogurt, flat bread, lettuce, tomato and cucumber.\n\nFunFAct:It is said that shish kebab was born over the open field fires of the soldiers of the Turkic tribes that first invaded Anatolia, who used their swords to grill meat, as they pushed west from their homelands in Central Asia.\n\nCalories:250",7.5f,"maindishes"));
		allitems.Add(new item(images[3],"Boiled Eggs","Two poached eggs, canadian bacon,black pepper,red chilli powedered. Half fried to maintain the taste and flavour.\nServed with hot bread.\n\nFunFact:organic eggs-should be cooked until the white and the yolk are firm, not runny.\n\nCalories:140 ",8.0f,"appetizers"));
		allitems.Add(new item(images[4],"Cold Drink 250ml","Carbonated Water, High Fructose Corn Syrup, Caramel Color, Phosphoric Acid, Natural Flavors, Caffeine.\n\nserved with fries\n\nfunfact: Coca cola is the second most widely understood term afte OK.\n\nCalories: 220 per can",2.0f,"appetizers"));
		allitems.Add(new item(images[5],"Icecream 1lbs","The perfect blend od choclate fudge with premium ice cream gives atactful delight for your temptation.\n\nserved with wafers.\n\nfunfact:It takes about 50 licks to finish a single scoop of icecream cone.\n\nCalories:290 per scoop",7.0f,"desserts"));
		allitems.Add(new item(images[6],"Sushi","15 pcs of sushi served with tuna roll and chef's special roll.It is pressed sushi, in which the fish is pressed onto the sushi rice. \n\nCalories:150 per piece",10.0f,"maindishes"));
		allitems.Add(new item(images[7],"Chicken Wrap complete meal","Marinated chicken breast fillet, grilled onion and grilled aubergine in your choice of peri peri with rocket and tasty hummus,wrapped with tortillas. \nServed with fries and nuggets.\n\nfunfact:Chickens will lay fewer and fewer but larger and larger eggs as they grow older.\n\nCalories:480",8.0f,"maindishes"));
		allitems.Add(new item(images[8],"Strawberry Pie","Cooked until thickened with sugar, cornstarch, water and added to it are freshly chopped strwberries. The crsut is added on top and the whole dish is chilled.\n\nFUNFACT: Eight strawberries provide more vitamin C than one orange! \n\nCalories:350 per piece",8.5f,"maindishes"));
		allitems.Add(new item(images[9],"veg Salad","Freshly chopped coriander, spinach, lattice wih turmeric dressing,beets,red onions,feta.\n\nfunfact: Lettuce is the second most popular fresh vegetable in the US behind potatoes.\n\nCalories:110",8.0f,"soups"));
		allitems.Add(new item(images[10],"Chicken Salad","Freshly chopped coriander, spinach, lattice and some finely chopped chicken pieces are added wih turmeric dressing,beets,red onions,feta.\n\nfunfact: In 2012, the world抯 largest non vegetarian salad and was made in Romania weighing in at 41,998 pounds.\n\nCalories:190",11.0f,"soups"));
		allitems.Add(new item(images[11],"Special Soup","Boiled vegetables and some ingredients such as meat and vegetables with stock, juice or water. Add ons like chilli sause and soya sauce are given. \nServed with bread sticks and cheese.\n \nfunfact:February 4th is National Homemade Soup Day.\nCalories:150",12.0f,"soups"));
		//allitems.Add(new item(images[7],"Black Forest Cake 1.5lbs","made with Choclate,butter and hooney",18.0f));
		//allitems.Add(new item(images[8],"Black Forest Cake 1.5lbs","made with Choclate,butter and hooney",18.0f));
		//allitems.Add(new item(images[9],"Black Forest Cake 1.5lbs","made with Choclate,butter and hooney",18.0f));
		//allitems.Add(new item(images[10],"Black Forest Cake 1.5lbs","made with Choclate,butter and hooney",18.0f));




	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void printorder()
	{
		Debug.Log ("Order type : " + appdata.type);
		foreach (string item in items) {
			Debug.Log (item);
		}
		Debug.Log ("__in the cart");
		foreach (item i in cart) {

			Debug.Log (i.name + " == " + i.details);
		}
	}


}

public class item
{
	public Sprite image;
	public string name;
	public string details;
	public float price;
	public string category;

	public item (Sprite img, string name, string details, float price,string cat)
	{
		this.image = img;
		this.name = name;
		this.details = details;
		this.price = price;
		this.category =cat;

	}

}

public class orderdetails
{
	public string name;
	public long phoneno;
	public string time;
	public string ordertype;
	public int orderid;
	public orderdetails(string n,long p,string t,string type)
	{
		name = n;
		phoneno = p;
		time = t;
		ordertype = type;
		orderid = 00000;
	}

	public orderdetails()
	{
		name = null;
		time = null;
	}


}
public class card
{
	long cardnumber;
	string cardholder;
	int cvv;
	string date;

	public card(long c,string h,int cv,string d)
	{
		cardnumber = c;
		cardholder = h;
		cvv = cv;
		date = d;
	}

}