﻿using UnityEngine;
using System.Collections;

public class ListController : MonoBehaviour {

	public GameObject content;
	public GameObject listitem;

	// Use this for initialization
	void Start () {

		foreach (item i in appdata.cart) {
			GameObject litem = Instantiate (listitem) as GameObject;
			ListItemController controller = litem.GetComponent<ListItemController> ();
			controller.icon.sprite = i.image;
			controller.title.text = i.name;
			controller.price.text = "$"+i.price.ToString();
			litem.transform.parent = content.transform;
			litem.transform.localScale = Vector3.one;
		
		
		}



	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
