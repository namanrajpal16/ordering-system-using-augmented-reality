﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class totalscript : MonoBehaviour {


	Text text;
	float total;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		text.text = "Total : $0";
		foreach (item i in appdata.cart) {
			total += i.price;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		text.text = "Total : $" + total;
	
	}
}
