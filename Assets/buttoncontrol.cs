﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class buttoncontrol : MonoBehaviour {


	public InputField IFname;
	public InputField IFnumber;
	public InputField IFtime;



	public InputField IFcardnumber;
	public InputField IFcardholder;
	public InputField IFcvv;
	public InputField IFmmyy;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	//functions for main screen
	public void scheduledelievery()
	{
		appdata.type = "delievery";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (3);//3 is for menulist
	}
	public void schedulepickup()
	{
		appdata.type = "pickup";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (3);
	}
	public void tablereservation()
	{
		appdata.type = "table";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (4);//four is for table reservation
	}
	public void orderrightnow()
	{
		appdata.type = "rightnow";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (3);
	}



	//these function are for menu scenes
	public void appetizers()
	{
		appdata.currenttype = "appetizers";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (8);//opening all
	}
	public void soups()
	{
		appdata.currenttype = "soups";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (8);
	}
	public void maindishes()
	{
		appdata.currenttype = "maindishes";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (8);
	}
	public void deserts()
	{
		appdata.currenttype = "desserts";
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (8);
	}


	//back button in menu scenes
	public void gobacktomenu()
	{
		SceneManager.LoadScene (3);
	}
	public void goback()
	{
		SceneManager.LoadScene (appdata.lastscene);
	}
	public void goHome()
	{
		appdata.type = null;
		appdata.items.Clear ();
		appdata.cart.Clear ();
		SceneManager.LoadScene (0);
	}


	//dynamic button or adding menu item to appdata.list, which is static
	//using buttons name as item name
	public void add()
	{
		//Debug.Log (EventSystem.current.currentSelectedGameObject.name);
		string iname = EventSystem.current.currentSelectedGameObject.name;
		appdata.items.Add (iname);
		item found = appdata.allitems.Find (item => item.name == iname); // using item name, we find the object from the appdata's list 
		Debug.Log ("Adding = " + found.name); // just for the console
		appdata.cart.Add(found);

	}


	//function to debug the printed order
	public void proceed()
	{
		appdata.printorder ();
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (7);//review page

	}

	public void confirm()
	{
		switch (appdata.type) {
		case "pickup":
			appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
			SceneManager.LoadScene (2);
			break;
		case "delievery":
			appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
			SceneManager.LoadScene (1);
			break;
		case "table":
			appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
			break;
		case "rightnow":
			appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
			break;
		default:
			break;

		}
	}



	public void AR()
	{
		SceneManager.LoadScene (6);
	}

	public void fetchdetails()
	{
		
		appdata.currentitem = EventSystem.current.currentSelectedGameObject.name;
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (5);//5 for detail page
	}




	public void gotopayment()
	{
		appdata.order.name = IFname.text;
		appdata.order.phoneno = long.Parse(IFnumber.text);
		appdata.order.time = IFtime.text;
		appdata.order.ordertype = appdata.type;
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (9);//9 for payment page
	}

	public void gotoconfirmation()
	{
		//getting data from the input fields if not null
		card c = new card (long.Parse (IFcardnumber.text), IFcardholder.text, int.Parse(IFcvv.text), IFmmyy.text);
		if (c != null) {
			
			appdata.order.orderid = Random.Range (10000, 30000);
		}

		if(long.Parse (IFcardnumber.text)!=null&&int.Parse(IFcvv.text)!=null&&IFmmyy.text!=null)
		{
		appdata.lastscene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (10);//9 for confirmation page
		}

	}

}
